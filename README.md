# ToS-BossMappin

### 準備
* config.batに環境変数を設定
 * ToSがインストールされてるフォルダー
 * IPFUnpacker.exeが入っているフォルダー

### ビルドのやり方
* 実装ファイル本体を編集します(data\\addon_d.ipf\\bossmapping以下のファイル)
* IPF Suite で bossmapping.ipf を開き更新したファイル下記の赤枠の中にD&Dします

![capture](IPFSuite capture.png)

* IPF Suite を閉じてbuild.batを実行します
* TOSを起動して確認
* 終了！

### アドオンを追加するにあたって

自己責任でお願いします。