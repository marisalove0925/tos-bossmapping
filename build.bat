rem @echo off
chcp 65001

call %~dp0config.bat

set ipffile=%~dp0data/BossMapping.ipf
call %IPF_UNPACKER% %ipffile% encrypt

copy "%~dp0data\\bossmapping.ipf" "%TOS_DIR%\\data\\_bossmapping_☕_v1.0.0.ipf"
xcopy /Y "%~dp0addons\\bossmapping" "%TOS_DIR%\\addons\\bossmapping"

pause
