
_G['ADDONS'] = _G['ADDONS'] or {};
_G["ADDONS"]["BOSSMAPPING"] = _G["ADDONS"]["BOSSMAPPING"] or {};
BossMapping = _G["ADDONS"]["BOSSMAPPING"];

local acutil = require("acutil");

function BOSSMAPPING_ON_INIT(addon, frame)

  if not BossMapping.isLoaded then
    CHAT_SYSTEM("BossMapping loaded!")
    CHAT_SYSTEM("BossMapping Start -> /bmstart")
    CHAT_SYSTEM("BossMapping Toggle Debug Mode -> /bmdebug")
    BossMapping.isLoaded = true;
  end

  BossMapping.isStarted = false;
  BossMapping.isDebug = false;

  acutil.slashCommand("/bmstart", BOSSMAPPING_SWITCH);
  acutil.slashCommand("/bmdebug", BOSSMAPPING_DEBUG_SWITCH);
  addon:RegisterMsg("FPS_UPDATE", "BOSSMAPPING_UPDATE");
  BOSSMAPPING_LOAD_SETTINGS();
end

function BOSSMAPPING_SAVE_SETTINGS()
  local tbl = {}
  tbl[ 0] = { name = "Blasphemous Deathweaver"; id = 47389 };
  tbl[ 1] = { name = "Bleak Chapparition"; id = 47392 };
  tbl[ 2] = { name = "Abomination"; id = 57500 };
  tbl[ 3] = { name = "Hungry Velnia Monkey"; id = 58237 };
  tbl[ 4] = { name = "Earth Templeshooter"; id = 57477 };
  tbl[ 5] = { name = "Earth Canceril"; id = 57478 };
  tbl[ 6] = { name = "Earth Archon"; id = 57479 };
  tbl[ 7] = { name = "Violent Cerberus"; id = 47391 };
  tbl[ 8] = { name = "Forest Keeper Ferret Marauder"; id = 58246 };
  tbl[ 9] = { name = "Kubas"; id = 57501 };
  tbl[10] = { name = "Noisy Mineloader"; id = 47393 };
  tbl[11] = { name = "Necroventer"; id = 57496 };
  tbl[12] = { name = "Burning Fire Lord"; id = 57711 };
  tbl[13] = { name = "Glackuman"; id = 57497 };
  tbl[14] = { name = "Wrathful Harpeia"; id = 57712 };
  tbl[15] = { name = "Marionette"; id = 57424 };
  tbl[16] = { name = "Dullahan"; id = 47498 };
  tbl[17] = { name = "Starving Ellaganos"; id = 57713 };
  tbl[18] = { name = "Prison Manager Prison Cutter"; id = 58068 };
  tbl[19] = { name = "Mirtis"; id = 58248 };
  tbl[20] = { name = "Rexipher"; id = 58250 };
  tbl[21] = { name = "Helgasercle"; id = 58249 };
  tbl[22] = { name = "Demon Lord Marnox"; id = 58251 };
  tbl[23] = { name = "Hallowventor"; id = 41281 };
  tbl[24] = { name = "Large Kepa"; id = 400002 };

  BossMapping.settings = tbl;
  acutil.saveJSON("../addons/bossmapping/settings.json", BossMapping.settings);
end

function BOSSMAPPING_LOAD_SETTINGS()
  local settings, error = acutil.loadJSON("../addons/bossmapping/settings.json");

  if error then
    BOSSMAPPING_SAVE_SETTINGS();
    --CHAT_SYSTEM("Create BossMapping Settings.json")
  else
    BossMapping.settings = settings;
  end
end

function BOSSMAPPING_UPDATE(frame, msg, argStr, argNum)
  if not BossMapping.isStarted then
    return;
  end

  local result = SCR_GET_ZONE_FACTION_OBJECT(session.GetMapName(), "Monster", "Normal/Elite/Boss", 120000);
  local currentChannel = session.loginInfo.GetChannel() + 1;
  
  for i,data in pairs(result) do
    local mobName = data[1];
    local monCls = GetClass("Monster", mobName);
    local className = monCls.ClassName;
    local classId = monCls.ClassID;
    
    for j, value in pairs(BossMapping.settings) do
      if tonumber(classId) == value.id then
         CHAT_SYSTEM( "Boss => " .. value.name .. " Found on channel " .. currentChannel .. "!");
        --imcSound.PlaySoundEvent("sys_levelup");
        return;
      end -- end of if classId == value.id
      if BossMapping.isDebug then
        CHAT_SYSTEM(className .. "[" .. classId .. "]");
      end
    end -- end of for j,value
    if BossMapping.isDebug then
      CHAT_SYSTEM("Frame Update.");
    end
  end --end of for i,data
end

function BOSSMAPPING_SWITCH()
    if not BossMapping.isStarted then
      BossMapping.isStarted = true;
      CHAT_SYSTEM("BossMapping Started!");
      return;
    end

    if BossMapping.isStarted then
      BossMapping.isStarted = false;
      CHAT_SYSTEM("BossMapping Stopped!");
      return;
    end
end

function BOSSMAPPING_DEBUG_SWITCH()
  if not BossMapping.isDebug then
    BossMapping.isDebug = true;
    CHAT_SYSTEM("BossMapping Debug[True]");
    return;
  end

  if BossMapping.isDebug then
    BossMapping.isDebug = false;
    CHAT_SYSTEM("BossMapping Debug[False]");
    return;
  end
end
