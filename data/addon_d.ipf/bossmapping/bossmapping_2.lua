local acutil = require("acutil");

_G['ADDONS'] = _G['ADDONS'] or {};
BossMapping = _G["ADDONS"]["BOSSMAPPING"] or {};

-- 初期化
function BOSSMAPPING_ON_INIT(addon, frame)

  BOSSMAPPING_LOAD_SETTINGS();
  addon:RegisterMsg("FPS_UPDATE", "FPS_ON_MSG_HOOKED");

  if not BossMapping.isLoaded then
    acutil.slashCommand("/bmdebug", BOSSMAPPING_DEBUG_SWITCH);
    BossMapping.isLoaded = true
    CHAT_SYSTEM("BossMapping loaded!")
    CHAT_SYSTEM("BossMapping Toggle Debug Mode -> /bmdebug")
  end
end

--毎フレーム実行する
function FPS_ON_MSG_HOOKED(frame, msg, argStr, argNum)
  local result = SCR_GET_ZONE_FACTION_OBJECT(session.GetMapName(), "Monster", "Normal/Material/Elite/Boss", 120000);

	for i,data in pairs(result) do
    
  end
end

-- モンスターをミニマップに表示する
function MAP_MON_MINIMAP_HOOKED(frame, msg, argStr, argNum, info)
  -- CHAT_SYSTEM("MAP_MON_MINIMAP_HOOKED Hocked")
  -- --_G["MAP_MON_MINIMAP_OLD"](frame, msg, argStr, argNum, info);
  --
  -- -- 最前面にミニマップが来ているか
  -- local isMinimap = false;
	-- if frame:GetTopParentFrame():GetName() == "minimap" then
	-- 	frame = GET_CHILD(frame, 'npclist', 'ui::CGroupBox');
	-- 	isMinimap = true;
	-- end
  --
  -- -- 何かのハンドル取得
  -- local ctrlName = "_MONPOS_" .. info.handle;
	-- local monPic = frame:GetChild(ctrlName);
	-- if monPic ~= nil then
	-- 	MAP_MON_MINIMAP_SETPOS(frame, info);
  --   CHAT_SYSTEM("monPic is null")
	-- 	return;
	-- end
  --
  -- -- マップ情報の取得
  -- local mapprop = session.GetCurrentMapProp();
  --
  -- -- プレイヤーでなければモンスター情報をとってくる（ＰｖＰのとき限定？）
	-- local isPC = info.type == 0;
	-- local monCls = nil;
	-- if false == isPC then
	-- 	monCls = GetClassByType("Monster", info.type);
	-- end
  --
  -- -- マップに表示する円の大きさを設定
  -- local width;
  -- local height;
  -- if isPC then
  --   width = 40;
  --   height = 40;
  -- else
  --   width = 120;
  --   height = 120;
  -- end
  --
  -- -- 円の画像更新
  -- local ctrlName = "_MONPOS_" .. info.handle;
	-- monPic = frame:CreateOrGetControl('picture', ctrlName, 0, 0, width, height);
	-- tolua.cast(monPic, "ui::CPicture");
	-- if false == monPic:HaveUpdateScript("_MONPIC_AUTOUPDATE") then
	-- 	monPic:RunUpdateScript("_MONPIC_AUTOUPDATE", 0);
	-- end
  --
  -- -- 円のパラメータ設定
  -- monPic:SetUserValue("W", width);
	-- monPic:SetUserValue("H", height);
	-- monPic:SetUserValue("HANDLE", info.handle);
	-- monPic:SetUserValue("EXTERN", "YES");
	-- monPic:SetUserValue("EXTERN_PIC", "YES");
  --
  -- -- ミニマップが開いている場合
	-- if isMinimap == true then
  --   local cursize = GET_MINIMAPSIZE();
  --   local dd = (100 + cursize) / 100;
  --   dd = 1 / dd;
  --   dd = CLAMP(dd, 0.5, 1.5);
  --   monPic:SetScale(dd, dd);
  -- end
  --
  -- -- プレイヤーの場合（PvP限定？)
  -- if isPC then
  --   monPic:SetEnableStretch(1);
  --   monPic:ShowWindow(1);
  --
  --   local myTeam = GET_MY_TEAMID();
  --   local outLineColor;
  --   if myTeam == info.teamID then
  --     outLineColor = "CCFFFFFF";
  --   else
  --     outLineColor = "CCCC0000";
  --   end
  --
  --   local imgName = GET_JOB_ICON(info.job);
  --   monPic:SetImage(imgName);
  --   monPic:ShowWindow(1);
  --   CHAT_SYSTEM("Why?isPC");
  -- else
  --   -- json内のクラスＩＤ検索
  --   local className = monCls.ClassName;
  --   local classId = monCls.ClassID;
  --   for i, value in pairs(BossMapping.settings) do
  --     if classId == value.id then
  --       SET_PICTURE_QUESTMAP(monPic, 30);
  --       ctrlName = "_MONPOS_T_" .. info.handle;
  --       local textC = frame:CreateOrGetControl('richtext', ctrlName, 0, 0, width, height);
  --       tolua.cast(textC, "ui::CRichText");
  --       textC:SetTextAlign("center", "bottom");
  --       textC:SetText("{@st42_yellow}" .. ClMsg("FieldBossAppeared!") .. "{nl}{@st42}" .. monCls.Name);
  --       textC:ShowWindow(1);
  --       textC:SetUserValue("EXTERN", "YES");
  --
  --       local mapName = mapprop:GetName();
  --       local currentChannel = session.loginInfo.GetChannel() + 1;
  --
  --       CHAT_SYSTEM(monCls.Name .. " is in [" .. mapName .. "] (" .. currentChannel .. ")");
  --       MAP_MON_MINIMAP_SETPOS(frame, info);
  --       return;
  --     end -- end of classId == value.Id
  --   end -- end of for
  --
  --   -- デバッグモード
  --   if BossMapping.isDebug then
  --     CHAT_SYSTEM(monCls.Name .. "[" .. classId .. "]" .. " is in [" .. mapName .. "] (" .. currentChannel .. ")");
  --   end
  --   CHAT_SYSTEM("Why?Debug");
  --
  --   local myTeam = GET_MY_TEAMID();
  --   if info.useIcon == true then
  --     monPic:SetImage(monCls.Icon);
  --   else
  --     if info.teamID == 0 then
  --       monPic:SetImage("fullyellow");
  --     elseif info.teamID ~= myTeam then
  --       monPic:SetImage("fullred");
  --     else
  --       monPic:SetImage("fullblue");
  --     end
  --   end
  --   monPic:SetEnableStretch(1);
  --   monPic:ShowWindow(1);
  -- end -- end of isPC else
  --
  -- MAP_MON_MINIMAP_SETPOS(frame, info);
end

function BOSSMAPPING_SAVE_SETTINGS()
  local tbl = {}
  tbl[ 0] = { name = "Blasphemous Deathweaver"; id = 47389 };
  tbl[ 1] = { name = "Bleak Chapparition"; id = 47392 };
  tbl[ 2] = { name = "Abomination"; id = 57500 };
  tbl[ 3] = { name = "Hungry Velnia Monkey"; id = 58237 };
  tbl[ 4] = { name = "Earth Templeshooter"; id = 57477 };
  tbl[ 5] = { name = "Earth Canceril"; id = 57478 };
  tbl[ 6] = { name = "Earth Archon"; id = 57479 };
  tbl[ 7] = { name = "Violent Cerberus"; id = 47391 };
  tbl[ 8] = { name = "Forest Keeper Ferret Marauder"; id = 58246 };
  tbl[ 9] = { name = "Kubas"; id = 57501 };
  tbl[10] = { name = "Noisy Mineloader"; id = 47393 };
  tbl[11] = { name = "Necroventer"; id = 57496 };
  tbl[12] = { name = "Burning Fire Lord"; id = 57711 };
  tbl[13] = { name = "Glackuman"; id = 57497 };
  tbl[14] = { name = "Wrathful Harpeia"; id = 57712 };
  tbl[15] = { name = "Marionette"; id = 57424 };
  tbl[16] = { name = "Dullahan"; id = 47498 };
  tbl[17] = { name = "Starving Ellaganos"; id = 57713 };
  tbl[18] = { name = "Prison Manager Prison Cutter"; id = 58068 };
  tbl[19] = { name = "Mirtis"; id = 58248 };
  tbl[20] = { name = "Rexipher"; id = 58250 };
  tbl[21] = { name = "Helgasercle"; id = 58249 };
  tbl[22] = { name = "Demon Lord Marnox"; id = 58251 };

  BossMapping.settings = tbl;
  acutil.saveJSON("../addons/bossmapping/settings.json", BossMapping.settings);
end

function BOSSMAPPING_LOAD_SETTINGS()
  local settings, error = acutil.loadJSON("../addons/bossmapping/settings.json");

  if error then
    BOSSMAPPING_SAVE_SETTINGS();
    --CHAT_SYSTEM("Create BossMapping Settings.json")
  else
    BossMapping.settings = settings;
  end
end

function BOSSMAPPING_DEBUG_SWITCH()
    if not BossMapping.isDebug then
      BossMapping.isDebug = true;
      CHAT_SYSTEM("BossMapping Debug[True]");
      return;
    end

    if BossMapping.isDebug then
      BossMapping.isDebug = false;
      CHAT_SYSTEM("BossMapping Debug[False]");
      return;
    end
end
